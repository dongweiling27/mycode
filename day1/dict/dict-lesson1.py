spidey = {
    "hero":"Spider-Man",
    "real name": "Peter Parker",
    "power":"shoot webs",
    "enemy":"Green Goblin"
}

print("Spider man is able to", spidey["power"])

# API documentation will provide the keys
print(spidey.keys())
print(spidey.values())