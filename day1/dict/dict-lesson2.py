spidey= {                                     
  "hero": "Spider-Man",                       
  "real name": "Peter Parker",                
  "power": [                                  
    "web shooting",                           
    "super strength",                         
    "spider sense"                            
  ],                                          
  "relationships": {                          
    "enemies": [                              
      "Green Goblin",                         
      "Vulture"                               
    ],                                        
    "allies": [                               
      "Deadpool",                             
      "Mary Jane"                             
    ]                                         
  }                                
}

# print out psiderman's first power:
print(spidey["power"][0])
# print out Green Goblin
print(spidey["relationships"]["enemies"][0])

# ADD ANOTHER POWER TO SPIDERMAN
print(spidey["power"])
spidey["power"].append("super agility")
print(spidey["power"])

# CHANGE SPIDERMAN'S REAL NAME TO MILES MORALES
print(spidey["real name"])
spidey["real name"] = "Mile Morales"
print(spidey["real name"])
spidey["real name"] = ["Mile Morales", "Peter Parker", "Kelly Dong"] # list of real names
spidey["real name"] = ("Mile Morales", "Peter Parker", "Kelly Dong") # tuple of real names
print(spidey["real name"])