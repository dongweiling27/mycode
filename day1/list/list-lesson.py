#.           0.            1.              2                3.            4
heros = ["Dr. Strange", "Spiderman", "Geralt of Rivia", "Iron Man", "Black Widow", "Deadpool"]
#            -6.            -5.        -4.                -3.         -2.            -1. 

    # SLICING - returning a portion of a larger object
    # lists use zero-based nueric indexing
    # slice to return a RANGE

    # [1:2:3] 
    # [START: STOP: STEP] STOP is non-inclusive
    # STEPS can be -1 (going backward)
print(heros[1])
print(heros[-1]) # last one in the list

# print(heros[-7]) INDEX OUT OF RANGE ERROR

# heros[::2] start from beginning go to the end step 2


# append
# extend

villains = ["Thanos", "Magneto"]
heros.append(villains)

# DIFFERENCE BTWN LIST AND TUPLE
# LISTS are MUTABLE -- able to changed 
# TUPLES are IMMUTABLE-- are NOT able to changed

print(heros.append(villains))

