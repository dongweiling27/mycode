#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests (get it?)
import requests

# URL = "https://swapi.dev/luke/force"      # Comment out this line
people_URL= "https://swapi.dev/api/people/4/"     # Uncomment this line
film_URL = "https://swapi.dev/api/films/1/"
starships_URL = "https://swapi.dev/api/starships/13/"

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "people_resp" object
    people_resp= requests.get(people_URL)
    film_resp= requests.get(film_URL)
    starships_resp= requests.get(starships_URL)

    # check to see if the status is anything other than what we want, a 200 OK
    if people_resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        vader= people_resp.json()
        pprint(vader)

    else:
        print("That is not a valid people URL.")

    if film_resp.status_code == 200:
        film= film_resp.json()
        pprint(film)

    else:
        print("That is not a valid film URL.")

    if starships_resp.status_code == 200:
        starships= starships_resp.json()
        pprint(starships)

    else:
        print("That is not a valid starships URL.")

    
    print(vader.get("name"), "was born in the year", 
        vader.get("birth-year"), ". His eyes are now", 
        vader.get("eye-color"), "and his hair color is",
        vader.get("hair_color"), ".")

    print("He first appeared in the movie", film.get("title"), 
        "and could be found flying around in his",
        starships.get("name"), ".")

if __name__ == "__main__":
    main()

