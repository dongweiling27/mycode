#!/usr/bin/env python3
"""Star Wars API HTTP response parsing"""

# requests is used to send HTTP requests (get it?)
import requests

URL= "https://swapi.dev/api/people/1"

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp= requests.get(URL)

    # what kind of python object is "resp"?
    print("This object class is:", type(resp), "\n")
    # response == what we get back from an API 
    # CLASS OBJECT

    # what can we do with it?
    print("Methods/Attributes include:", dir(resp))

    # ATTRIBUTE = piece of metadata stored in the obejct
    # METHOD = piece of code executed by the object

    print(resp.status_code) # attribute
    print(resp.url) # attribute
    data = resp.json() # method
        # stripping the json off the response
        # converting the json into python code
        # decoding into utf-8
    print(data)

if __name__ == "__main__":
    main()
