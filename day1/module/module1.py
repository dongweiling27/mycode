print("Hello World")

print("The value of __name__ in module1 is", __name__)

# are you running this code directly?
if __name__ == "__main__":
    print("The value of __name__ in module1 is", __name__)

## WHY we need to know this???

def function1():
    print("Hello")


def function2():
    print("Goodbye")


def function3():
    print("Sup?")

# when python commands are if __name__ then when someone else is running function 
# from your file it won't call those functions that are within the if statement therefore
# saving time
if __name__ == "__main__":
    function1()
    function2()
    function3()