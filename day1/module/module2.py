import module1

print("The value of __name__ in module2 is", __name__)

# RESULT:
    # The value of __name__in module1 is module1
    # The value of __name__ in module2 is __main__
    # note: name of mod1 is not main but mod1


module1.function1()