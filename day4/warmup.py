x="Slappy"

@app.route("/example/<y>")
def example1(y):
    return f"Hello {y}!"

@app.route("/example2")
def example2():
    return redirect("/example1/Happy")

@app.route("/example3")
def example3():
    print("Hello, world!")
    return "World, hello!"


"""
1. What would the output of this command return:
    curl localhost:2224/example1/PythonStudent

Hello PythonStudent

2. If a GET request were to sent to /example2, 
    what requests/respsonse codes would you see?

GET/example2 300, GET /example1 200

3. If a POST request were to sent to /example3, 
    what reposne code would you see?

something in the 400 error. (405) 

4. What you see in aux1 if you went to /example3?

World, hello!

5. If you need to know if the incoming rweuqest was a GET or a POST,
which "request" attribute would you use?

request.method



"""