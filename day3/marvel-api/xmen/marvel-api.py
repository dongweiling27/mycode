import requests
import time
import hashlib

API = 'https://gateway.marvel.com/v1/public/characters'

# build and return
def hashbuilder(ts, privkey, pubkey):
    combination = f"{ts}{privkey}{pubkey}"
    combination = combination.encode("utf-8")
    print(combination)
    hashyhash = hashlib.md5(combination)
    print(hashyhash.hexdigest())
    return hashyhash.hexdigest()

# read pirv/pub key (saved in the current directory)
with open("marvel.priv") as pkey:
    privkey = pkey.read().rstrip('\n')

with open("marvel.pub") as pkey:
    pubkey = pkey.read().rstrip('\n')

# create a timestamp
ts = str(time.time())

#calling a custom function
hashyhash = hashbuilder(ts, privkey, pubkey)

print("PubKey:", pubkey)
print("Privkey:", privkey)
print("Time:", ts)
print("Hash:", hashyhash)

final = f"{API}?ts={ts}&apikey={pubkey}&hash={hashyhash}"
print(final)