# create a parser

parser.add_argument('--drink',
                    metavar='NAME',
                    type=str,
                    default='coffee',
                    help="Pick a drink. Default is 'coffee'")