#!/usr/bin/python3
# An object of Flask class is our WSGI application
from flask import Flask

# Flask constructor takes the name of current
# module (__name__) as argument
app = Flask(__name__)
# the flask object (app) represents our whole API!
# "teach" the app object how to behave


# route() function of the Flask class is a
# decorator, tells the application which URL
# should call the associated function
# @<from line 7>
@app.route("/") #api landing page
def hello_world():
   return "Hello World"

if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224) # runs the application
   # app.run(host="0.0.0.0", port=2224, debug=True) # DEBUG MODE
