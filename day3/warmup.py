import requests
import datetime
import reverse_geocoder as rg
    
URL= "http://api.open-notify.org/iss-now.json"
def main():
    resp= requests.get(URL).json()
    lon = resp.get("iss_position").get("longitude")
    lat = resp.get("iss_position").get("latitude")
    epoch_time = resp.get("timestamp")
    human_time = datetime.datetime.utcfromtimestamp(epoch_time).replace(tzinfo=datetime.timezone.utc)
    result = rg.search((lat, lon))


    print("CURRENT LOCATION OF THE ISS:")
    print("Timestamp:", human_time)
    print("Lon:", lon)
    print("Lat:", lat)
    print("City/Country:", result[0].get('name'), result[0].get('cc'))



if __name__ == "__main__":
    main()