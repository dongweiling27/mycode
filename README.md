# My Code

This is a Python API training hosted by Alta3 10/17/22 - 10/21/22.
Repository contains source code by Kelly Dong during this training.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

How to Install the Language: Python

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Kelly Dong** - *Bny Mellon*
