heroes= ["Spiderman",                                    
         "Green Goblin",                                 
         {"power": "web shooters"}]                      
                                                          
# CONCATENATE                                             
# Spiderman just shot Green Goblin with his webshooters!  
hero= heroes[0]                                           
villain= heroes[1]                                        
power= heroes[2]["power"]                                 
                                                          
# F-STRING example!                                       
print(f"{hero} just shot {villain} with his {power}!") 
print(f"{heroes[0]} just shot {heroes[1]} with his {heroes[2]['power']}!") 