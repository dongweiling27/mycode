from getkey import getkey, key

print("Press Enter to continue\n")
resp = getkey()
if resp == key.ENTER:
    print("ENTER sandman!")
elif resp == "n":
    print("n is for NEXT!")
else:
    print("you did not type enter or 'n'. Quitting...")
    exit(1)

print("Thanks for playing!")